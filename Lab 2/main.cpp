#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <GL/glew.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <Cg/cg.h>
#include <Cg/cgGL.h>

struct VertexProgramParameters
{
	CGparameter modelView;
	CGparameter modelViewProj;
	CGparameter wsize;
	CGparameter near;
	CGparameter top;
	CGparameter bottom;
};

struct FragmentProgramParameters
{
    CGparameter unproj_scale;
    CGparameter unproj_offset;
    CGparameter near;
    CGparameter zb_scale;
    CGparameter zb_offset;
    CGparameter epislon;
};

CGcontext	context;
CGprofile	vertexProfile, fragmentProfile;

CGprogram vertexProgram, fragmentProgram;
VertexProgramParameters vertexParams;
FragmentProgramParameters fragmentParams;

int w_width=512, w_height=512;

bool animate = true;

struct Surfel
{
    float pos[3];
    float color[4];
    float uvec[3];
    float vvec[3];
};

#include "points2.h"

void handleCgError() 
{
    fprintf(stderr, "Cg error: %s\n", cgGetErrorString(cgGetError()));
}

void assertNoGLError(size_t line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR) {
        fprintf(stderr, "Something inside OpenGL went wrong while drawing on line %lu: %s\n", line, gluErrorString(err));
        exit(1);
	}
}

void setPerspective(double fov_y, double aspect, double z_near, double z_far)
{
	double frustum_height = tan( fov_y / 360 * M_PI) * z_near;
	double frustum_width = frustum_height * aspect;
	
	glFrustum(
		-frustum_width, frustum_width,
		-frustum_height, frustum_height,
		z_near, z_far);

    // Also send these frustum parameters to the vertex & fragment programs
    cgGLSetParameter1f(vertexParams.near, z_near);
    cgGLSetParameter1f(vertexParams.bottom, -frustum_height);
    cgGLSetParameter1f(vertexParams.top, frustum_height);

    cgGLSetParameter1f(fragmentParams.near, z_near);
    
    // map w_width [0..512] to [-frustum_width .. frustum_width]
    // >> wpos * 1/512 * 2 * frustum_width
    cgGLSetParameter2f(fragmentParams.unproj_scale,
        (1.0 / w_width)  * 2 * frustum_width,
        (1.0 / w_height) * 2 * frustum_height);

    // And offset it in such a way that it 0,0 is in the center of the window
    cgGLSetParameter2f(fragmentParams.unproj_offset, frustum_width, frustum_height);
}

void display() 
{
    int curTime = glutGet(GLUT_ELAPSED_TIME);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    setPerspective(30.0f, (float)w_width/w_height, 0.1, 100.0);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(4, 0, -3, 0, 0, 0, 0, 1, 0);
    
    if (animate)
        glRotatef((float)curTime/20.0, 0, 1, 0);
    
    // Enable vertex shader
    cgGLEnableProfile(vertexProfile);
    cgGLBindProgram(vertexProgram);

    // Enable fragment shader
    cgGLEnableProfile(fragmentProfile);
    cgGLBindProgram(fragmentProgram);
    
    assertNoGLError(__LINE__);
    
    // Bind stuff to the vertex shader
    
    cgGLSetStateMatrixParameter(vertexParams.modelView,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_IDENTITY);
    
    cgGLSetStateMatrixParameter(vertexParams.modelViewProj,
        CG_GL_MODELVIEW_PROJECTION_MATRIX,
        CG_GL_MATRIX_IDENTITY);
        
    cgGLSetParameter2f(vertexParams.wsize, w_width, w_height);

	

    glClientActiveTexture(GL_TEXTURE0);
    glTexCoordPointer(4, GL_FLOAT, sizeof(Surfel), &pts[0].color);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glClientActiveTexture(GL_TEXTURE1);
    glTexCoordPointer(3, GL_FLOAT, sizeof(Surfel), &pts[0].uvec);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glClientActiveTexture(GL_TEXTURE2);
    glTexCoordPointer(3, GL_FLOAT, sizeof(Surfel), &pts[0].vvec);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	assertNoGLError(__LINE__);

    // Draw stuff
	glVertexPointer(3, GL_FLOAT, sizeof(Surfel), &pts[0].pos);
    glEnableClientState(GL_VERTEX_ARRAY);

    assertNoGLError(__LINE__);
    
    glDrawArrays(GL_POINTS, 0, numpoints);

    assertNoGLError(__LINE__);

    glDisableClientState(GL_VERTEX_ARRAY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    cgGLDisableProfile(vertexProfile);
    
	assertNoGLError(__LINE__);
    
	glutSwapBuffers();
}

/* Choose profiles, set optimal options */
void chooseCgProfiles()
{
    vertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
    cgGLSetOptimalOptions(vertexProfile);
    fragmentProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
    cgGLSetOptimalOptions(fragmentProfile);
    printf("vertex profile:   %s\n",
         cgGetProfileString(vertexProfile));
    printf("fragment profile: %s\n",
         cgGetProfileString(fragmentProfile));

}

/* Load Cg program from disk */
bool loadCgProgram(CGprofile profile, const char *filename, CGprogram &target)
{
    CGprogram program;
    assert(cgIsContext(context));

    fprintf(stderr, "Cg program %s creating.\n", filename);
    program = cgCreateProgramFromFile(context, CG_SOURCE,
            filename, profile, NULL, NULL);
    
    if(!cgIsProgramCompiled(program)) {
        printf("%s\n",cgGetLastListing(context));
        return false;
    }
    
    fprintf(stderr, "Cg program %s loading.\n", filename);
    cgGLLoadProgram(program);
    
    target = program;
    return true;
}


bool loadCgPrograms()
{
    if (!loadCgProgram(vertexProfile, "vertex_program.cg", vertexProgram))
        return false;

    vertexParams.modelView = cgGetNamedParameter(vertexProgram, "modelView");
    vertexParams.modelViewProj = cgGetNamedParameter(vertexProgram, "modelViewProj");
    vertexParams.wsize = cgGetNamedParameter(vertexProgram, "wsize");
    vertexParams.near = cgGetNamedParameter(vertexProgram, "near");
    vertexParams.top = cgGetNamedParameter(vertexProgram, "top");
    vertexParams.bottom = cgGetNamedParameter(vertexProgram, "bottom");
    
    if (!loadCgProgram(fragmentProfile, "fragment_program.cg", fragmentProgram))
        return false;

    fragmentParams.unproj_scale = cgGetNamedParameter(fragmentProgram, "unproj_scale");
    fragmentParams.unproj_offset = cgGetNamedParameter(fragmentProgram, "unproj_offset");
    fragmentParams.near = cgGetNamedParameter(fragmentProgram, "near");

    return true;
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key) {
        case 'q':
        case 'Q':
        case 27: // escape
            exit(0);
            break;

        case 32: // spacebar
            animate = !animate;
            break;

        case 'r': // reload
            loadCgPrograms();
            break;
    }
}

void idle()
{
    glutPostRedisplay();
}

void reshape(int width, int height)
{
    w_width = width;
    w_height = height;
    glViewport(0, 0, w_width, w_height);
}

void close()
{
    exit(0);
}

int main(int argc, char *argv[])
{	
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow("Point based rendering in Cg");
    
    glewInit();
    
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutReshapeFunc(reshape);
    
    #ifdef __APPLE__
	glutWMCloseFunc(close);
	#endif
    
    cgSetErrorCallback(handleCgError);
    context = cgCreateContext();
    chooseCgProfiles();
    
    if (!loadCgPrograms())
        return 1;
    
    glutMainLoop();
    
    return 0;
}


