#include <iostream>
#include <GL/glew.h>
#include <Cg/cg.h>
#include <Cg/cgGL.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int w_width=512, w_height=512;

CGcontext context = NULL;
CGprogram myVertexProgram = NULL;
CGprofile vertexProfile = CG_PROFILE_VP40;

CGparameter modelViewProj = NULL;
CGparameter modelView = NULL;
CGparameter modelViewIT = NULL;

CGprogram myFragmentProgram = NULL;
CGprofile fragmentProfile = CG_PROFILE_FP40;

CGparameter lightdir = NULL;

float light_x = 0.5;
float light_y = 0.5;
float light_z = 1.0;

static void catchGLError(size_t line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR)
        std::cerr << "Something inside OpenGL went wrong while drawing on line " << line << ": " << gluErrorString(err) << std::endl;
}

static void mouse(int button, int state, int x, int y)
{
    switch (button)
    {
        case GLUT_LEFT_BUTTON:
            if (state == GLUT_DOWN) {
                light_x = (float) x / 512;
                light_y = (float) y / 512;
            }
            break;
    }

    glutPostRedisplay();
}

static void display()
{
    cgGLEnableProfile(vertexProfile);
    cgGLBindProgram(myVertexProgram);

    cgGLEnableProfile(fragmentProfile);
    cgGLBindProgram(myFragmentProgram);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cgGLSetStateMatrixParameter(modelViewProj,
        CG_GL_MODELVIEW_PROJECTION_MATRIX,
        CG_GL_MATRIX_IDENTITY);

    cgGLSetStateMatrixParameter(modelView,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_IDENTITY);

    cgGLSetStateMatrixParameter(modelViewIT,
        CG_GL_MODELVIEW_MATRIX,
        CG_GL_MATRIX_INVERSE_TRANSPOSE);

    cgGLSetParameter3f(lightdir, light_x, light_y, light_z);

    catchGLError(__LINE__);

    // glutSolidSphere(3.0, 50, 50);
    glutSolidTeapot(1.0);

    catchGLError(__LINE__);

    cgGLDisableProfile(fragmentProfile);
    cgGLDisableProfile(vertexProfile);

    glutSwapBuffers();
}

static void initializeGlut(int *argc, char *argv[])
{
    glutInit(argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w_width, w_height);
    glutCreateWindow(argv[0]);

    glewInit();

    glutMouseFunc(mouse);

    glutDisplayFunc(display);

    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(
        90.0, // fov
        1.0,  // aspect ratio
        .05,  // near
        40);  // far

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        0, -5, 5, // eye
        0, 0, 0, // lookat point
        0, 1, 0);// up
}

static bool initializeCGVertexProgram(const std::string& source_file)
{
    if (!cgGLIsProfileSupported(vertexProfile)) {
        std::cerr << "The vertex profile is not supported" << std::endl;
        return false;
    }

    myVertexProgram = cgCreateProgramFromFile(
        context,
        CG_SOURCE,
        source_file.c_str(),
        vertexProfile,
        "main", // name of entry point in our shader
        NULL);

    if (!myVertexProgram) {
        std::cerr << "Couldn't load vertex program:\n" << cgGetLastListing(context) << std::endl;
        return false;
    }

    cgGLLoadProgram(myVertexProgram);

    modelViewProj = cgGetNamedParameter(myVertexProgram, "modelViewProj");

    modelView = cgGetNamedParameter(myVertexProgram, "modelView");

    modelViewIT = cgGetNamedParameter(myVertexProgram, "modelViewIT");

    if (!modelViewProj || !modelView || !modelViewIT) {
        std::cerr << "Parameter modelViewProj, modelView or modelViewIT was not defined in the vertex shader" << std::endl;
        return false;
    }

    return true;
}

static bool initializeCGFragmentProgram(const std::string& source_file)
{
    if (!cgGLIsProfileSupported(fragmentProfile)) {
        std::cerr << "The fragment profile is not supported" << std::endl;
        return false;
    }

    myFragmentProgram = cgCreateProgramFromFile(
        context,
        CG_SOURCE,
        source_file.c_str(),
        fragmentProfile,
        "main", // name of entry point in our shader
        NULL);

    if (!myFragmentProgram) {
        std::cerr << "Couldn't load fragment program:\n" << cgGetLastListing(context) << std::endl;
        return false;
    }

    cgGLLoadProgram(myFragmentProgram);

    lightdir = cgGetNamedParameter(myFragmentProgram, "lightdir");

    if (!lightdir) {
        std::cerr << "Parameter lightdir was not defined in the fragment shader" << std::endl;
        return false;
    }

    return true;
}

static bool initializeCG()
{
    cgGLSetDebugMode(true);

    context = cgCreateContext();
    
    return initializeCGVertexProgram("colorful_v.cg")
        && initializeCGFragmentProgram("colorful_f.cg");
}

int main (int argc, char *argv[])
{
    initializeGlut(&argc, argv);
    
    if (!initializeCG())
        return -1;

    if (glGetError() != GL_NO_ERROR) {
        std::cerr << "Something inside OpenGL went wrong." << std::endl;
        return -2;
    }

    glutMainLoop();
        
    // never gets here.
    return 0;
}
